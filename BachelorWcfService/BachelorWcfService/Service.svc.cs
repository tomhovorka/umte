﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Services.Description;
using BachelorWcfService.DataList;
using BachelorWcfService.Helper;

namespace BachelorWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service.svc or Service.svc.cs at the Solution Explorer and start debugging.
    public class Service : IService
    {

        public string HelloWorld()
        {

            var a = AppDomain.CurrentDomain.BaseDirectory;

            Directory.CreateDirectory($"{AppDomain.CurrentDomain.BaseDirectory}/Files");

            return a;
        }

        public int UploadFile(string byteArray, string filename)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(byteArray);
            return (int)FileHelper.SaveFile(base64EncodedBytes, filename);
        }


        public string DownloadFile(string fileName)
        {
            return FileHelper.ExistFile(fileName) == Enums.SaveResults.FileNotExist 
                                                            ? FileHelper.GetUrlOfFile(fileName) 
                                                            : Enums.GetDescriptionFromEnumValue(Enums.SaveResults.FileNotExist);
        }

        public List<string> GetAvailableFilesList()
        {
            return FileHelper.GetAvailableFiles();
        }

        public Dictionary<int, string> GetUploadResults()
        {
            var enums = Enum.GetValues(typeof(Enums.SaveResults)).Cast<Enums.SaveResults>().ToList();

            return enums.ToDictionary(results => (int)results, results => Enums.GetDescriptionFromEnumValue(results));
        }

        public string ClearFolder()
        {
            FileHelper.ClearFolder();

            return "Smazáno";
        }

        public const string ConnectionString = "Data Source=blue.globenet.cz;Initial Catalog=d001301;Integrated Security=False;User ID=d001301a;Connect Timeout=15;Encrypt=False;Packet Size=4096;Password=A6rF5tJ3wV";


        public string InsertMobileToStatistics(string osVersion, string imei)
        {
            
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Select Number From [dbo].[Imei]";
                    cmd.CommandType = CommandType.Text;

                    SqlDataReader reader = cmd.ExecuteReader();

                    bool isUssed = false;
                    while (reader.Read())
                    {
                        if (imei == Convert.ToString(reader[0]))
                            isUssed = true;
                    }

                    reader.Close();

                    if (!isUssed)
                    {

                        cmd.CommandText = $"INSERT INTO [dbo].[Imei] ([Number]) VALUES ('{imei}')";

                        cmd.ExecuteNonQuery();


                        cmd.CommandText = "Select Version, Id, Count From [dbo].[ConnectedDevice]";
                        cmd.CommandType = CommandType.Text;

                        reader = cmd.ExecuteReader();

                        bool isLogged = false;
                        int id = 0;
                        int newVersionCount = 0;
                        while (reader.Read())
                        {
                            if (osVersion == Convert.ToString(reader[0]))
                            {
                                isLogged = true;
                                id = Convert.ToInt32(reader[1]);
                                newVersionCount = Convert.ToInt32(reader[2]) + 1;
                            }
                        }
                        reader.Close();

                        cmd.CommandText = isLogged ? $"UPDATE [dbo].[ConnectedDevice] SET Count = {newVersionCount} WHERE Id = '{id}'" : $"INSERT INTO [dbo].[ConnectedDevice] ([Version], [Count]) VALUES ('{osVersion}', 1)";

                        cmd.ExecuteNonQuery();

                        return Enums.GetDescriptionFromEnumValue(Enums.SaveResults.Success);
                    }
                }
            }


            return Enums.GetDescriptionFromEnumValue(Enums.SaveResults.ImeiLogged);
        }

        public Dictionary<string, int> GetLoggedMobiles()
        {
            Dictionary<string, int> toReturn = new Dictionary<string, int>();
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Select Version, Count From [dbo].[ConnectedDevice]";
                    cmd.CommandType = CommandType.Text;

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        toReturn.Add(Convert.ToString(reader[0]), Convert.ToInt32(reader[1]));
                    }
                }
            }

            return toReturn;
        }



        public bool Ping()
        {
            return true;
        }


    }
}
