﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;

namespace BachelorWcfService.DataList
{
    public class Enums
    {
        public enum SaveResults
        {
            [Description("Vše proběhlo v pořádku.")]
            Success = 0,

            [Description("Překročena maximální možná velikost souboru.")]
            MaxSizeExceed = 1,

            [Description("Soubor s tímto názvem již existuje.")]
            FileNameDuplicated = 2,

            [Description("Neočekávaná chyba, opakujte prosím znovu.")]
            UnexpectedError = 3,

            [Description("Ve jméně souboru se nacházejí nepodporavetlené znaky.")]
            NotSupportedCharsInFileName = 4,

            [Description("Vybraný soubor neexistuje.")]
            FileNotExist = 5,

            [Description("Toto zařízení bylo již uloženo do databáze.")]
            ImeiLogged = 6,

        }

        public static string GetDescriptionFromEnumValue(Enum value)
        {

            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value.ToString();
            }

            EnumMemberAttribute attribute = value.GetType()
                .GetField(value.ToString())
                .GetCustomAttributes(typeof(EnumMemberAttribute), false)
                .SingleOrDefault() as EnumMemberAttribute;
            return attribute == null ? value.ToString() : attribute.Value;
        }

    }
}