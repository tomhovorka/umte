﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BachelorWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
    [ServiceContract]
    public interface IService
    {

        #region API - hello world

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "helloworld/")]
        string HelloWorld();


        #endregion API - hello world

        #region API - file operations

        /// <summary>
        /// method expect file converted to bytearray and save it to local disk
        /// </summary>
        /// <param name="byteArray">file converted to byte array</param>
        /// <param name="filename"></param>
        /// <returns>result code</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "uploadfile/")]
        int UploadFile(string byteArray, string filename);

        /// <summary>
        /// method expect file name and return uri to file location
        /// </summary>
        /// <param name="filename">name of file to download</param>
        /// <returns>uri of file</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "downloadfile/{filename}")]
        string DownloadFile(string filename);

        /// <summary>
        /// method provide get list of files located on service
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "getavailablefileslist/")]
        List<string> GetAvailableFilesList();

        /// <summary>
        /// method provide list of result what can become when you upload file to service
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "getuploadresults/")]
        Dictionary<int, string> GetUploadResults();

        /// <summary>
        /// method provide list of result what can become when you upload file to service
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "clearfolder/")]
        string ClearFolder();

        #endregion API - file operations

        #region API - db operations

        /// <summary>
        /// method expect file converted to bytearray and save it to local disk
        /// </summary>
        /// <param name="byteArray">file converted to byte array</param>
        /// <returns>result code</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "insertmobiletostatistics/")]
        //string InsertMobileToStatistics();
        string InsertMobileToStatistics(string osVersion, string imei);

        /// <summary>
        /// method expect file converted to bytearray and save it to local disk
        /// </summary>
        /// <param name="byteArray">file converted to byte array</param>
        /// <returns>result code</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "getloggedmobiles/")]
        Dictionary<string, int> GetLoggedMobiles();



        #endregion API - db operations

        #region API - ping

        /// <summary>
        /// check if service is available
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "ping/")]
        bool Ping();

        #endregion API - ping
    }
}
