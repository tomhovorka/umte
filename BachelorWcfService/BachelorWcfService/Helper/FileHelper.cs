﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.CompensatingResourceManager;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using BachelorWcfService.DataList;

namespace BachelorWcfService.Helper
{
    public static class FileHelper
    {
        private static string GetFileFolder()
        {
            return $"{AppDomain.CurrentDomain.BaseDirectory}/Files/";
        }


        public static Enums.SaveResults SaveFile(byte[] byteArray, string filename)
        {
            if (!Regex.Match(filename, "^[^\\/:*?\"<>|]*$").Success)
            {
                return Enums.SaveResults.NotSupportedCharsInFileName;
            }

            if (byteArray.Length > 2097152)
                return Enums.SaveResults.MaxSizeExceed;

            try
            {
                if (File.Exists($"{GetFileFolder()}{filename}"))
                    return Enums.SaveResults.FileNameDuplicated;

                File.WriteAllBytes($"{GetFileFolder()}{filename}", byteArray);
                return Enums.SaveResults.Success;

            }
            catch (Exception ex)
            {
                return Enums.SaveResults.UnexpectedError;
            }
        }

        public static Enums.SaveResults ExistFile(string fileName)
        {
            if (File.Exists($"{GetFileFolder()}{fileName}"))
                return Enums.SaveResults.FileNotExist;

            return Enums.SaveResults.Success;
        }

        public static string GetUrlOfFile(string fileName)
        {
            StringBuilder sb = new StringBuilder();
            string rootUlr = System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.UriTemplateMatch.BaseUri.ToString().Replace(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.UriTemplateMatch.BaseUri.LocalPath.ToString(), "");

            return $"{rootUlr}/Files/{fileName}";
        }

        public static List<string> GetAvailableFiles()
        {
            return Directory.GetFiles(GetFileFolder()).Select(Path.GetFileName).ToList();
        }

        public static void ClearFolder()
        {
            DirectoryInfo di = new DirectoryInfo(GetFileFolder());

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
        }
    }
}