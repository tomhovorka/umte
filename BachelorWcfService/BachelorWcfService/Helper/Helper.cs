﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace BachelorWcfService.Helper
{
    public static class Helper
    {
        public static T Deserialize<T>(this String s)
        {
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            T input = json_serializer.Deserialize<T>(s);
            return input;
        }

        public static String Serialize(this object s)
        {
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            return json_serializer.Serialize(s);
        }
    }
}