package com.bachelor.tomas.client.Interfaces;

import android.view.View;

/**
 * Created by Tomas on 22.03.2018.
 */

public interface IdownloadItemClick {
    void onClick(View view, int position);
}
