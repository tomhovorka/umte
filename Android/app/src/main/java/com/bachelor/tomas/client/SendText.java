package com.bachelor.tomas.client;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.bachelor.tomas.client.Helpers.SimpleListItemAdapter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class SendText extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_file);

        setTitle("Odeslat zprávu");


        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if(!bluetoothAdapter.isEnabled()){
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BLUETOOTH);
        }

        findViewByIds();

        writePairedDevices();

        hideSendingViews();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ClientClass clientClass = new ClientClass(btArray[i]);

                clientClass.start();
            }
        });

        sendBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String string = String.valueOf(writeMsg.getText());
                sendReceive.write(string.getBytes());
                writeMsg.setText("");
                writeMsg.clearFocus();
            }
        });


    }

    BluetoothAdapter bluetoothAdapter;
    BluetoothDevice[] btArray;

    Button sendBtn;
    ListView listView;
    TextView status;

    static final int STATE_LISTENING = 1;
    static final int STATE_CONNECTING = 2;
    static final int STATE_CONNECTED = 3;
    static final int STATE_CONNECTIONFAILED = 4;

    int REQUEST_ENABLE_BLUETOOTH = 1;

    private void writePairedDevices(){
        Set<BluetoothDevice> bt = bluetoothAdapter.getBondedDevices();
        String[] strings = new String[bt.size()];
        btArray = new BluetoothDevice[bt.size()];
        int index = 0;

        if(bt.size() > 0){

            for (BluetoothDevice device : bt){
                btArray[index] = device;
                strings[index] = device.getName();
                index++;
            }

            ArrayAdapter<String> adapter = new SimpleListItemAdapter(getApplicationContext(), strings);

            listView.setAdapter(adapter);
        }
    }

    private void findViewByIds(){
        listView = (ListView) findViewById(R.id.listView);
        status = (TextView) findViewById(R.id.status);
        writeMsg = (EditText) findViewById(R.id.editmessage);
        sendBtn = (Button) findViewById(R.id.send);
    }

    private void hideSendingViews(){
        writeMsg.setVisibility(View.GONE);
        sendBtn.setVisibility(View.GONE);
    }

    private void showSendingViews(){
        writeMsg.setVisibility(View.VISIBLE);
        sendBtn.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
    }


    private static final UUID MY_UUID = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");

    Send sendReceive;

    EditText writeMsg;

    private class ClientClass extends Thread{

        private BluetoothDevice device;
        private BluetoothSocket socket;

        public ClientClass(BluetoothDevice device1){
            device = device1;

            try {
                socket = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run(){

            try {
                socket.connect();
                Message message = Message.obtain();
                message.what = STATE_CONNECTED;
                handler.sendMessage(message);

                sendReceive = new Send(socket);
            } catch (IOException e) {
                e.printStackTrace();
                Message message = Message.obtain();
                message.what = STATE_CONNECTIONFAILED;
                handler.sendMessage(message);
            }

        }

    }

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case STATE_LISTENING:
                    status.setText("čekání na připojení");
                    break;
                case STATE_CONNECTING:
                    status.setText("připojování");
                    break;
                case STATE_CONNECTED:
                    status.setText("připojeno");
                    showSendingViews();
                    break;
                case STATE_CONNECTIONFAILED:
                    status.setText("připojení selhalo");
                    break;
            }

            return true;
        }
    });

    private class Send extends Thread{

        private final BluetoothSocket socket;
        private final OutputStream outputStream;

        public Send(BluetoothSocket socket){
            this.socket = socket;
            OutputStream tmpOut = null;


            try {
                tmpOut = this.socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            outputStream = tmpOut;

        }

        public void write(byte[] bytes){
            try {
                outputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
