package com.bachelor.tomas.client;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ChoosePeerToPeerAction extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_peer_to_peer_action);

        setTitle("Peer-to-Peer");
    }

    public void receiveFile(View view){
        Intent intent = new Intent(this, ReceiveText.class);

        startActivity(intent);
    }

    public void sendFile(View view){

        Intent intent = new Intent(this, SendText.class);

        startActivity(intent);
    }
}
