package com.bachelor.tomas.client.Helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bachelor.tomas.client.R;

import java.util.ArrayList;

/**
 * Created by Tomas on 09.04.2018.
 */

public class SimpleListItemAdapter extends ArrayAdapter<String> {

    public SimpleListItemAdapter(Context context, String[] items){
        super(context, 0, items);
    }

    public SimpleListItemAdapter(Context context, ArrayList<String> items){
        super(context, 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        String item = getItem(position);

        if(convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.simple_list_view_item, parent, false);

        ((TextView) convertView.findViewById(R.id.filename)).setText(item);

        return convertView;
    }
}
