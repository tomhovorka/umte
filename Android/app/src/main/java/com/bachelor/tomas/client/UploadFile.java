package com.bachelor.tomas.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bachelor.tomas.client.Helpers.ErrorMessageProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static java.lang.String.*;

public class UploadFile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_file);

        fileName = ((TextView) findViewById(R.id.fileName));
        chooseFileBtn = ((View) findViewById(R.id.chooseFile));
        uploadFileBtn = ((View) findViewById(R.id.uploadFile));
        newFileName = ((EditText) findViewById(R.id.newFileName));
        fileNameLabel = ((TextView) findViewById(R.id.fileNameLabel));
        description = ((TextView) findViewById(R.id.manual));
        result = ((TextView) findViewById(R.id.result));

        setCompleteViewsInvisible();

        setTitle("Nahrát soubor");

        ErrorMessageProvider.loadData(this);

    }

    private void setCompleteViewsInvisible() {
        description.setVisibility(View.GONE);
        uploadFileBtn.setVisibility(View.GONE);
        newFileName.setVisibility(View.GONE);
        fileNameLabel.setVisibility(View.GONE);
        fileName.setVisibility(View.GONE);
    }

    private void setCompleteViewsVisible() {
        description.setVisibility(View.VISIBLE);
        uploadFileBtn.setVisibility(View.VISIBLE);
        newFileName.setVisibility(View.VISIBLE);
        newFileName.setText("");
        fileNameLabel.setVisibility(View.VISIBLE);
        result.setVisibility(View.VISIBLE);
        result.setTextColor(Color.BLACK);
        fileName.setVisibility(View.VISIBLE);
    }

    private void setPrepareViewsInvisible() {
        chooseFileBtn.setVisibility(View.INVISIBLE);
    }

    private void setPrepareViewsVisible() {
        chooseFileBtn.setVisibility(View.VISIBLE);
    }

    private void setSaveResult(int resultCode) {
        result.setText(ErrorMessageProvider.getMessage(getApplicationContext(), resultCode));

        if (resultCode == 0)
            result.setTextColor(Color.GREEN);
        else
            result.setTextColor(Color.RED);
    }

    public void chooseFile(View view) {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        intent.addCategory(Intent.CATEGORY_OPENABLE);

        intent.setType("*/*");

        startActivityForResult(intent, READ_REQUEST_CODE);

        result.setText("");
    }


    byte[] byteArray;
    String byteArrays;
    Uri uri = null;
    String url = "http://test.goldensupport.cz/Service.svc/uploadfile/";
    TextView fileName, result;
    View chooseFileBtn, uploadFileBtn, fileNameLabel, description;
    EditText newFileName;

    private static final int READ_REQUEST_CODE = 42;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            uri = null;
            if (resultData != null) {
                uri = resultData.getData();

                try {
                    fileName.setText(getFileName(uri));

                    if (uri != null)
                        setCompleteViewsVisible();

                    Toast.makeText(this, "Soubor byl uložen do paměti.", Toast.LENGTH_LONG).show();


                } catch (Exception ex) {
                    Toast.makeText(this, "Při výběru souboru nastal problém, opakujte prosím znovu.", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public String getFileName(Uri uri) {
        String result = null;

        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }

        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }

        return result;
    }


    public void uploadFile(View view) throws IOException {

        final RequestQueue requestQueue = Volley.newRequestQueue(this);


        Bitmap bmp = getBitmapFromUri(uri);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byteArray = stream.toByteArray();

        byteArrays = android.util.Base64.encodeToString(byteArray, 0);


        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("byteArray", byteArrays);
        jsonParams.put("filename", getNewFileName());

        JsonObjectRequest myRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                new JSONObject(jsonParams),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            setSaveResult(response.getInt("UploadFileResult"));
                            setCompleteViewsInvisible();
                            fileNameLabel.setVisibility(View.INVISIBLE);

                        } catch (JSONException e) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Při odesílání souboru nastal problém, opakujte prosím znovu.", Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", "My useragent");
                return headers;
            }

        };

        View viewForFocus = this.getCurrentFocus();
        if (viewForFocus != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(viewForFocus.getWindowToken(), 0);
        }

        requestQueue.add(myRequest);
    }


    private String getNewFileName() {

        String s = newFileName.getText().toString();
        if (s.length() == 0)
            return fileName.getText().toString();

        String[] splitted = fileName.getText().toString().split("\\.");
        String lastOne = splitted[splitted.length - 1];

        return newFileName.getText().toString() + "." + lastOne;
    }

    private String getNewFileNameWithoutSuffix() {

        if (newFileName.getText().toString() == "") {

            String[] splitted = fileName.getText().toString().split("\\.");

            String nameWithoutSuffix = "";
            for (int i = 0; i < splitted.length - 2; i++) {
                nameWithoutSuffix += splitted[i];
            }

            return nameWithoutSuffix;

        }

        return newFileName.getText().toString();

    }
}
