package com.bachelor.tomas.client;

import android.Manifest;
import android.app.DownloadManager;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bachelor.tomas.client.Helpers.RecyclerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DownloadFile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_file);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);


        arrayList = pokus(this);

        setTitle("Stáhnout soubor");

    }

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<String> arrayList;


    private ArrayList<String> pokus(final DownloadFile parrentClass) {
        final RequestQueue requestQueue = Volley.newRequestQueue(this);


        final ArrayList<String> arrayList = new ArrayList<>();

        JsonObjectRequest requestl = new JsonObjectRequest(Request.Method.GET, "http://test.goldensupport.cz/Service.svc/getavailablefileslist/", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray data = response.getJSONArray("GetAvailableFilesListResult");
                    int count = 0;
                    while (count < data.length()) {
                        try {
                            String jsonObject = data.getString(count);
                            arrayList.add(jsonObject);
                            count++;
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }

                    adapter = new RecyclerAdapter(arrayList, getApplicationContext(), parrentClass);
                    recyclerView.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Zkontrolujte internetové připojení.", Toast.LENGTH_LONG);
            }
        });

        requestQueue.add(requestl);

        return arrayList;
    }


    public void downloadFile(int position) {
        Toast.makeText(getApplicationContext(), "Váš soubor se nyní začne stahovat.", Toast.LENGTH_SHORT).show();
        download(arrayList.get(position));
    }


    private void download(final String filename) {


        final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, "http://test.goldensupport.cz/Service.svc/downloadfile/" + filename, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    haveStoragePermission();

                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(response.getString("DownloadFileResult")));
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename);
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    request.allowScanningByMediaScanner();
                    DownloadManager manager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                    manager.enqueue(request);
                } catch (Exception ex) {

                }

                requestQueue.stop();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                requestQueue.stop();
            }
        });

        requestQueue.add(request);


    }

    private boolean haveStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //you dont need to worry about these stuff below api level 23
            return true;
        }
    }


    private void getFileUrl(String url) {

    }
}
