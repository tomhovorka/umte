package com.bachelor.tomas.client.Helpers;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Tomas on 22.03.2018.
 */

public class ErrorMessageProvider {

    private static HashMap<Integer, String> dictionary;

    private static final String url = "http://test.goldensupport.cz/Service.svc/getuploadresults/";

    public static String getMessage(final Context context, int code) {

        if (dictionary == null || dictionary.size() == 0) {
            loadData(context);
        }

        return dictionary.get(code);
    }

    public static void loadData(final Context context){

        final RequestQueue requestQueue = Volley.newRequestQueue(context);

        dictionary = new HashMap<>();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, (String) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                try {
                    JSONArray jArray = response.getJSONArray("GetUploadResultsResult");
                    for (int i = 0; i < jArray.length(); i++) {
                        try {
                            JSONObject oneObject = jArray.getJSONObject(i);

                            int key = oneObject.getInt("Key");
                            String value = oneObject.getString("Value");

                            dictionary.put(key, value);

                        } catch (JSONException e) {

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                requestQueue.stop();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, "loading service result failed.", Toast.LENGTH_LONG).show();

                error.printStackTrace();
                requestQueue.stop();
            }
        });

        requestQueue.add(request);
    }
}
