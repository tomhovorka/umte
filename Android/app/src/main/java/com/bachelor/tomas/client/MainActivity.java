package com.bachelor.tomas.client;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.bachelor.tomas.client.Helpers.ErrorMessageProvider;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Klientská aplikace");

        ErrorMessageProvider.loadData(getApplicationContext());
    }

    public void openServerActivity(View view){

        Intent intent = new Intent(this, ChooseServerAction.class);

        startActivity(intent);
    }

    public void openPeerToPeerActivity(View view){

        Intent intent = new Intent(this, ChoosePeerToPeerAction.class);

        startActivity(intent);
    }
}
