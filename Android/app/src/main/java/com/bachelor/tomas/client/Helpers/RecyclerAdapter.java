package com.bachelor.tomas.client.Helpers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bachelor.tomas.client.DownloadFile;
import com.bachelor.tomas.client.Interfaces.IdownloadItemClick;
import com.bachelor.tomas.client.R;

import java.util.ArrayList;

/**
 * Created by Tomas on 22.03.2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    ArrayList<String> items = new ArrayList<>();
    Context context;
    DownloadFile parrentClass;

    public RecyclerAdapter(ArrayList<String> items, Context context, DownloadFile parrentClass) {
        this.items = items;
        this.context = context;
        this.parrentClass = parrentClass;
    }


    @Override
    public RecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_list_view_item, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.MyViewHolder holder, int position) {
        holder.textView.setText(items.get(position));

        holder.setItemClickListener(new IdownloadItemClick() {
            @Override
            public void onClick(View view, int position) {
                parrentClass.downloadFile(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }




    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textView;

        private IdownloadItemClick clickListener;

        public MyViewHolder(View itemView) {
            super(itemView);

            textView = (TextView) itemView.findViewById(R.id.filename);

            itemView.setOnClickListener(this);
        }

        public void setItemClickListener(IdownloadItemClick clickListener) {
            this.clickListener = clickListener;
        }


        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getAdapterPosition());
        }
    }
}
