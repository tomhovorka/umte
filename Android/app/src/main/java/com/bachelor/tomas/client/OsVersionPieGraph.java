package com.bachelor.tomas.client;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bachelor.tomas.client.Helpers.PieChartFormatter;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OsVersionPieGraph extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_os_version_pie_graph);

        getImeiInfo();
        drawGraph();

        setTitle("Databáze");
    }

    private String getImeiInfo() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {

                TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                return telephonyManager.getDeviceId();

            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);

                return "";
            }
        }

        return "";
    }

    public void logMyDeviceToServer(View view) {
        final RequestQueue requestQueue = Volley.newRequestQueue(this);

        String imei = getImeiInfo();

        if (imei == "") {
            Toast.makeText(this, "Neumožnil jste povolení práv pro tuto akci.", Toast.LENGTH_LONG).show();
            return;
        }

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("osVersion", Build.VERSION.RELEASE);
        jsonParams.put("imei", getImeiInfo());

        JsonObjectRequest myRequest = new JsonObjectRequest(
                Request.Method.POST,
                "http://test.goldensupport.cz/Service.svc/insertmobiletostatistics/",
                new JSONObject(jsonParams),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Toast.makeText(getApplicationContext(), response.getString("InsertMobileToStatisticsResult"), Toast.LENGTH_LONG).show();
                            drawGraph();
                        } catch (JSONException e) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", "My useragent");
                return headers;
            }

        };

        requestQueue.add(myRequest);
    }

    private void drawGraph() {


        final RequestQueue requestQueue = Volley.newRequestQueue(this);


        JsonObjectRequest requestl = new JsonObjectRequest(Request.Method.GET, "http://test.goldensupport.cz/Service.svc/getloggedmobiles/",
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray data = response.getJSONArray("GetLoggedMobilesResult");

                            List<PieEntry> pieEntries = new ArrayList<>();

                            int count = 0;
                            while (count < data.length()) {

                                try {

                                    JSONObject jsonObject = data.getJSONObject(count);

                                    pieEntries.add(new PieEntry(jsonObject.getInt("Value"), jsonObject.getString("Key")));

                                    count++;
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }

                                PieDataSet dataSet = new PieDataSet(pieEntries, "");
                                dataSet.setValueFormatter(new PieChartFormatter());
                                dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
                                dataSet.setValueTextSize(16);
                                PieData pieData = new PieData(dataSet);

                                PieChart chart = (PieChart) findViewById(R.id.graph);
                                chart.setDrawSliceText(false);
                                chart.getDescription().setText("Verze systému");
                                chart.getDescription().setTextSize(14);

                                chart.setData(pieData);

                                chart.invalidate();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "blbý", Toast.LENGTH_LONG);
                    }
                });

        requestQueue.add(requestl);

    }
}
