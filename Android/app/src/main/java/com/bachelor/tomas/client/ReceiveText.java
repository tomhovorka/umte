package com.bachelor.tomas.client;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bachelor.tomas.client.Helpers.SimpleListItemAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.UUID;

public class ReceiveText extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_file);

        setTitle("Přijmout zprávu");

        status = (TextView) findViewById(R.id.status);
        receivedTexts = (ListView) findViewById(R.id.listView);
        arrayList = new ArrayList<String>();

        adapter = new SimpleListItemAdapter(getApplicationContext(), arrayList);

        receivedTexts.setAdapter(adapter);


        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        ServerClass serverClass = new ServerClass();
        serverClass.start();
    }

    static final int STATE_LISTENING = 1;
    static final int STATE_CONNECTING = 2;
    static final int STATE_CONNECTED = 3;
    static final int STATE_CONNECTIONFAILED = 4;
    static final int STATE_MESSAGE_RECEIVED = 5;

    int REQUEST_ENABLE_BLUETOOTH = 1;

    BluetoothAdapter bluetoothAdapter;

    TextView status;
    ListView receivedTexts;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;

    Receive receive;


    private static final String APP_NAME = "BT chat";
    private static final UUID MY_UUID = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            switch (message.what){
                case STATE_LISTENING:
                    status.setText("čekání na připojení");
                    break;
                case STATE_CONNECTING:
                    status.setText("připojování");
                    break;
                case STATE_CONNECTED:
                    status.setText("připojeno");
                    break;
                case STATE_CONNECTIONFAILED:
                    status.setText("připojení selhalo");
                    break;
                case STATE_MESSAGE_RECEIVED:
                    byte[] readBuff = (byte[]) message.obj;
                    String tempMsg = new String(readBuff, 0, message.arg1);
                    arrayList.add(tempMsg);
                    adapter.notifyDataSetChanged();
                    break;
            }

            return true;
        }
    });

    private class  ServerClass extends Thread{

        private BluetoothServerSocket serverSocket;

        public ServerClass(){
            try {
                serverSocket = bluetoothAdapter.listenUsingRfcommWithServiceRecord(APP_NAME, MY_UUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        public void run(){
            BluetoothSocket socket = null;

            while (socket == null){
                try{
                    Message message = Message.obtain();
                    message.what = STATE_LISTENING;
                    handler.sendMessage(message);

                    socket = serverSocket.accept();
                } catch (IOException ex){
                    ex.printStackTrace();

                    Message message = Message.obtain();
                    message.what = STATE_CONNECTIONFAILED;
                    handler.sendMessage(message);
                }

                if(socket != null){
                    Message message = Message.obtain();
                    message.what = STATE_CONNECTED;
                    handler.sendMessage(message);

                    receive = new Receive(socket);
                    receive.start();

                    break;
                }
            }


        }

    }


    private class Receive extends Thread{

        private final BluetoothSocket socket;
        private final InputStream inputStream;

        public Receive(BluetoothSocket socket){
            this.socket = socket;
            InputStream tmpIn = null;


            try {
                tmpIn = this.socket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            inputStream = tmpIn;

        }

        public void run(){
            byte[] buffer = new byte[1024];
            int bytes;

            while(true){

                try {
                    bytes = inputStream.read(buffer);
                    handler.obtainMessage(STATE_MESSAGE_RECEIVED, bytes, -1, buffer).sendToTarget();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
