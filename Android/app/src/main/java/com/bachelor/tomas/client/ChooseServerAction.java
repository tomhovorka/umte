package com.bachelor.tomas.client;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ChooseServerAction extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_server_action);

        setTitle("Client - Server");
    }

    public void uploadFile(View view){

        Intent intent = new Intent(this, UploadFile.class);

        startActivity(intent);
    }

    public void downloadFile(View view){

        Intent intent = new Intent(this, DownloadFile.class);

        startActivity(intent);
    }

    public void deviceInfo(View view){

        Intent intent = new Intent(this, OsVersionPieGraph.class);

        startActivity(intent);
    }
}
